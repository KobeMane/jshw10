const container = document.querySelector(".centered-content");
const list = document.querySelector(".tabs");
const heroes = [...list.children];
const heroesStories = document.querySelector(".tabs-content");
const heroStory = [...heroesStories.children];

heroStory.forEach((element) => {
  element.hidden = true;
});
heroes.forEach((element) => {
  element.classList.remove("active");
});

storyTell(heroes, heroStory);

function storyTell(element, story) {
  for (const el of element) {
    el.addEventListener("click", function () {
      element.forEach((element) => {
        element.classList.remove("active");
      });

      el.classList.add("active");

      if (el.innerText) {
        for (const e of story) {
          if (e.innerText.includes(el.innerText)) {
            story.forEach((element) => {
              element.hidden = true;
            });

            e.hidden = false;
          }
        }
      }
    });
  }
}


// heroStory.forEach((element) => {
//   element.style.display = "none";
// });
// heroes.forEach((element) => {
//   element.classList.remove("active");
// });

// jopa(heroes, heroStory);

// function jopa(element, story) {
//   for (const el of element) {
//     el.addEventListener("click", function () {
//       element.forEach((element) => {
//         element.classList.remove("active");
//       });

//       el.classList.add("active");

//       if (el.innerText) {
//         for (const e of story) {
//           if (e.innerText.includes(el.innerText)) {
//             story.forEach((element) => {
//               element.style.display = "none";
//             });

//             e.style.display = "block";
//           }
//         }
//       }
//     });
//   }
// }



